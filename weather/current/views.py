from django.shortcuts import render
from django.http import HttpResponse
#from .forms import UserForm
#from .models import user
import requests
import json
def weather(m):
    if m.method == 'POST':
        city=m.POST['s']
        url ='http://api.openweathermap.org/data/2.5/weather?q={}&appid=08cdb440e8dc6e643882f9472b1c6b77&units=metric'.format(city)
        res= requests.get(url)
        mapurl='https://www.google.com/maps/embed/v1/place?key=AIzaSyAjxgZlU8pwN4EqU8nolPGPTUbyOlHFef0&q={}'.format(city)
        data = res.json()
        x=data['cod']
        alpha="sorry this city is not present"
        if x== 200:
            temp =data['main']['temp']
            min_temp=data['main']['temp_min']
            description=data['weather'][0]['description']
            icon=data['weather'][0]['icon']
            max_temp=data['main']['temp_max']
            wind_Speed=data['wind']['speed']
            lat=data['coord']['lat']
            long=data['coord']['lon']

            return render(m,'weather.html',{'a':temp,'b':max_temp,'c':min_temp,'city':city,'icon':icon,'description':description,'lat':lat,'long':long,'map':mapurl})
        else:
            return render(m,'weather.html',{'alpha':alpha,'map':mapurl})
    return render(m,'weather.html')
